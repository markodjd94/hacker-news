import { Injectable } from "@angular/core";
import { Observable, BehaviorSubject } from "rxjs";
import { map } from "rxjs/operators";
import { StoryIds } from "../models/StoryIds";
import { HttpClient } from "@angular/common/http";
import { Story } from "../models/Story";
import { User } from "../models/User";
import { Comment } from "../models/Comment";

@Injectable({
  providedIn: "root",
})
export class HackerNewsService {
  baseUrl: string = "https://hacker-news.firebaseio.com/v0/";

  titleSource = new BehaviorSubject("default message");
  currentTitle = this.titleSource.asObservable();

  constructor(private http: HttpClient) {}

  getTopStories(search: string): Observable<StoryIds> {
    return this.http.get(`${this.baseUrl}/${search}.json`).pipe(
      map((data) => {
        return new StoryIds(data);
      })
    );
  }

  getStory(id: number): Observable<Story> {
    return this.http.get(`${this.baseUrl}/item/${id}.json`).pipe(
      map((data) => {
        return new Story(data);
      })
    );
  }

  getComment(id: number): Observable<Comment> {
    return this.http.get(`${this.baseUrl}/item/${id}.json`).pipe(
      map((data) => {
        return new Comment(data);
      })
    );
  }

  getUser(id: string): Observable<User> {
    return this.http.get(`${this.baseUrl}/user/${id}.json`).pipe(
      map((data) => {
        return new User(data);
      })
    );
  }

  changeTitle(message: string) {
    this.titleSource.next(message);
  }
}
