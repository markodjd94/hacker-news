import { Component, OnInit, Input } from "@angular/core";
import { StoryIds } from "../models/StoryIds";
import { HackerNewsService } from "../services/hacker-news.service";
import { Story } from "../models/Story";

@Component({
  selector: "app-story",
  templateUrl: "./story.component.html",
  styleUrls: ["./story.component.scss"],
})
export class StoryComponent implements OnInit {
  @Input() id: number;
  story: Story;

  constructor(private service: HackerNewsService) {}

  ngOnInit(): void {
    this.renderStory();
  }

  renderStory() {
    if (this.id) {
      this.service.getStory(this.id).subscribe((data) => {
        this.story = data;
      });
    }
  }
}
