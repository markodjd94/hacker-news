import { Component, OnInit } from "@angular/core";
import { HackerNewsService } from "../services/hacker-news.service";

@Component({
  selector: "app-title",
  templateUrl: "./title.component.html",
  styleUrls: ["./title.component.scss"],
})
export class TitleComponent implements OnInit {
  title: string;
  constructor(private service: HackerNewsService) {}

  ngOnInit(): void {
    this.getTitle();
  }

  getTitle() {
    this.service.currentTitle.subscribe((message) => (this.title = message));
  }
}
