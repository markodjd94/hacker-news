export class Comment {
  by: string;
  id: number;
  parent: number;
  text: string;
  time: number;
  type: string;

  constructor(obj?: any) {
    this.by = (obj && obj.by) || "";
    this.id = (obj && obj.id) || null;
    this.parent = (obj && obj.parent) || null;
    this.text = (obj && obj.text) || "";
    this.time = (obj && obj.time) || null;
    this.type = (obj && obj.type) || null;
  }
}
