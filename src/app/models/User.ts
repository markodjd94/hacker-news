export class User {
  about: string;
  created: number;
  id: string;
  karma: number;
  submitted: number[];

  constructor(obj?: any) {
    this.about =
      (obj && obj.about) || "User didn't write anything about himself/herself";
    this.created = (obj && obj.created) || null;
    this.id = (obj && obj.id) || null;
    this.karma = (obj && obj.karma) || 0;
    this.submitted = (obj && obj.submitted) || [];
  }
}
