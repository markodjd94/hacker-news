import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { UserComponent } from "./user/user.component";
import { StoriesComponent } from "./stories/stories.component";
import { CommentsComponent } from "./comments/comments.component";

const routes: Routes = [
  {
    path: "home",
    component: StoriesComponent,
  },
  { path: "home/:stories", component: StoriesComponent },
  { path: "user/:id", component: UserComponent },
  { path: "comments/:id", component: CommentsComponent },
  { path: "", redirectTo: "/home", pathMatch: "full" },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
