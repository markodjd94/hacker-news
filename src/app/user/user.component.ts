import { Component, OnInit } from "@angular/core";
import { HackerNewsService } from "../services/hacker-news.service";
import { ActivatedRoute } from "@angular/router";
import { User } from "../models/User";

@Component({
  selector: "app-user",
  templateUrl: "./user.component.html",
  styleUrls: ["./user.component.scss"],
})
export class UserComponent implements OnInit {
  user: User;
  title: string;

  constructor(
    private service: HackerNewsService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.newTitle();
    this.renderUser();
  }

  renderUser() {
    let id = this.route.snapshot.params["id"];
    this.service.getUser(id).subscribe((data) => {
      this.user = data;
    });
  }

  newTitle() {
    this.service.changeTitle("User Info");
  }
}
