import { Component, OnInit } from "@angular/core";
import { HackerNewsService } from "src/app/services/hacker-news.service";
import { StoryIds } from "src/app/models/StoryIds";
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"],
})
export class HomeComponent implements OnInit {
  constructor(private router: Router, private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.router.navigate(["top-stories"], { relativeTo: this.route });
  }
}
