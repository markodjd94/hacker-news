import { Component, OnInit, Input } from "@angular/core";
import { Comment } from "../models/Comment";
import { HackerNewsService } from "../services/hacker-news.service";

@Component({
  selector: "app-comment",
  templateUrl: "./comment.component.html",
  styleUrls: ["./comment.component.scss"],
})
export class CommentComponent implements OnInit {
  @Input() commentId: number;
  comment: Comment;
  show: boolean = false;

  constructor(private service: HackerNewsService) {}

  ngOnInit(): void {
    this.renderComment();
  }

  renderComment() {
    this.service.getComment(this.commentId).subscribe((data) => {
      this.comment = data;
    });
  }

  showHide() {
    this.show = !this.show;
  }
}
