import { Component, OnInit } from "@angular/core";
import { HackerNewsService } from "../services/hacker-news.service";
import { StoryIds } from "../models/StoryIds";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: "app-stories",
  templateUrl: "./stories.component.html",
  styleUrls: ["./stories.component.scss"],
})
export class StoriesComponent implements OnInit {
  ids: StoryIds;
  title: string;
  search: string;

  constructor(
    private service: HackerNewsService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.renderStories();
    this.newTitle();
  }

  renderStories() {
    let stories = this.route.snapshot.params["stories"];

    if (stories === "newstories") {
      this.search = "newstories";
    } else {
      this.search = "topstories";
      this.router.navigate(["home"]);
    }
    this.service.getTopStories(this.search).subscribe((data) => {
      this.ids = data;
    });
  }

  newTitle() {
    if (this.search === "newstories") {
      this.title = "New Stories";
    } else {
      this.title = "Top Stories";
    }
    this.service.changeTitle(this.title);
  }
}
