import { Component, OnInit } from "@angular/core";
import { Story } from "../models/Story";
import { HackerNewsService } from "../services/hacker-news.service";
import { ActivatedRoute } from "@angular/router";
import { switchMap } from "rxjs/operators";

@Component({
  selector: "app-comments",
  templateUrl: "./comments.component.html",
  styleUrls: ["./comments.component.scss"],
})
export class CommentsComponent implements OnInit {
  story: Story;
  title: string = "Comments";

  constructor(
    private service: HackerNewsService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.renderStory();
    this.newTitle();
  }

  renderStory() {
    let id = this.route.snapshot.params["id"];

    if (id) {
      this.service.getStory(id).subscribe((data) => {
        this.story = data;
      });
    }
  }

  newTitle() {
    this.service.changeTitle(this.title);
  }
}
