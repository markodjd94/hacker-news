import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { NavComponent } from "./core/nav/nav.component";
import { HomeComponent } from "./core/home/home.component";
import { StoriesComponent } from "./stories/stories.component";
import { StoryComponent } from "./story/story.component";
import { HttpClientModule } from "@angular/common/http";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ScrollingModule } from "@angular/cdk/scrolling";
import { MatIconModule } from "@angular/material/icon";
import { UserComponent } from "./user/user.component";
import { TitleComponent } from "./title/title.component";
import { CommentsComponent } from "./comments/comments.component";
import { CommentComponent } from "./comment/comment.component";
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    HomeComponent,
    StoriesComponent,
    StoryComponent,
    UserComponent,
    TitleComponent,
    CommentsComponent,
    CommentComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ScrollingModule,
    MatIconModule,
    MatProgressSpinnerModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
